﻿using UnityEngine;

public class FingerBehaviour : MonoBehaviour {

    [SerializeField]
    private Vector3 speed;

    private void Update() {

        if(Input.GetKey(KeyCode.W)) {
            transform.Translate(Vector3.forward * speed.z * Time.deltaTime);
        }

        if(Input.GetKey(KeyCode.S)) {
            transform.Translate(Vector3.back * speed.z * Time.deltaTime);
        }

        if(Input.GetKey(KeyCode.A)) {
            transform.Translate(Vector3.left * speed.x * Time.deltaTime);
        }

        if(Input.GetKey(KeyCode.D)) {
            transform.Translate(Vector3.right * speed.x * Time.deltaTime);
        }

        if(Input.GetKey(KeyCode.Q)) {
            transform.Translate(Vector3.up * speed.y * Time.deltaTime);
        }

        if(Input.GetKey(KeyCode.E)) {
            transform.Translate(Vector3.down * speed.y * Time.deltaTime);
        }

    }

}